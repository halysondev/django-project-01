# Internationalization
# https://docs.djangoproject.com/en/4.2/topics/i18n/

from .environment import BASE_DIR

LANGUAGE_CODE = "pt-br"

TIME_ZONE = "America/Fortaleza"

USE_I18N = True

USE_TZ = True

LOCALE_PATHS = [
    BASE_DIR / "locale",
]
