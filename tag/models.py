import string
from random import SystemRandom

from django.db import models
from django.urls import reverse
from django.utils.text import slugify


class Tag(models.Model):
    name = models.CharField(max_length=255)
    slug = models.SlugField(unique=True)

    # Campos com a relação genéricas
    # Represanta o model com relação genérica
    # content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)

    # Represanta o id genérico
    # object_id = models.CharField(max_length=255)

    # Um campo que representa a relação genérica
    # content_object = GenericForeignKey("content_type", "object_id")

    def save(self, *args, **kwargs):
        if not self.slug:
            rand_letters = "".join(
                SystemRandom().choices(
                    string.ascii_letters + string.digits,
                    k=5,
                )
            )

            self.slug = slugify(f"{self.name}-{rand_letters}")
        return super().save(*args, **kwargs)

    class Meta:
        verbose_name = "Tag"
        verbose_name_plural = "Tags"

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("Tag_detail", kwargs={"pk": self.pk})
