import os
from typing import Any

from django.db.models import Count, Q
from django.db.models.query import QuerySet
from django.forms.models import model_to_dict
from django.http import Http404, HttpResponse, JsonResponse
from django.shortcuts import render
from django.views.generic import DetailView, ListView

from recipes.models import Recipe
from tag.models import Tag
from utils.pagination import make_pagination

PER_PAGE = int(os.environ.get("PER_PAGE", 9))


def theory(request):
    """recipes = Recipe.objects.all()
    recipes = recipes.filter(title__icontains="Teste")

    recipes = Recipe.objects.filter(
        Q(
            title__icontains="da",
            id__gt=2,
            is_published=True,
        )
        | Q(),
    )

    recipes = Recipe.objects.filter(
        id=F("author__id"),
    )[:10]

    ### Retorna o proprio id da recipe igual ao id do autor ###
    recipes = Recipe.objects.filter(
        id=F("author__id"),
    ).order_by("-id", "title")


    ### Com esse campos values a gente faz o select do sql
    e retorna um dict ###


    recipes = Recipe.objects.values("id")

    ### Com esse campos only faz a mesma coisa do values, mas retorna
    os valores do model, cuidado que se eu pedir um valor que ele não listou
    vai ficar muito lento ###

    recipes = Recipe.objects.only("id", "title")

    ### Com esse campos defer faz a mesma coisa do values, mas não retorna
    os valor que eu selecionei, e tem o mesmo problema do only ###

    recipes = Recipe.objects.defer("id", "title")

    ### Com o aggregate é possivel criar fields com metodos do postgresql
    como max, min, count, etc ###

    number_of_recipes = recipes.aggregate(amount=Count("id"))


    ### Esse metodo vem do customer manager que eu crie no model,
    assim eu posso fazer consultas usando o ORM de forma mais fácil ###

    Recipe.objects.get_published()"""

    recipes = Recipe.objects.get_published()

    number_of_recipes = recipes.aggregate(amount=Count("id"))["amount"]

    context = {
        "recipes": recipes,
        "amount": number_of_recipes,
    }

    return render(
        request,
        "recipes/pages/theory.html",
        context=context,
    )


class RecipeListViewBase(ListView):
    model = Recipe
    paginate_by = None
    # Objeto que eu passo para a view
    context_object_name = "recipes"
    ordering = ["-id"]

    # Nome do template que vamos utilizar, por padrão vem recipe_list
    template_name = "recipes/pages/home.html"

    # Alterando o queryset para filtrar nossa queryset

    def get_queryset(self) -> QuerySet[Any]:
        queryset = super().get_queryset()

        queryset = queryset.filter(
            is_published=True,
        )

        queryset.select_related(
            "authors",
            "category",
            "authors__profile",
        )
        queryset.prefetch_related("tags")

        return queryset

    #  Alterando o contexto que vai ser retornado

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context_data = super().get_context_data(**kwargs)
        page_obj, pagination_range = make_pagination(
            self.request,
            context_data.get("recipes"),
            PER_PAGE,
        )
        context_data.update(
            {
                "recipes": page_obj,
                "pagination_range": pagination_range,
            }
        )
        return context_data


class RecipeListViewHome(RecipeListViewBase):
    template_name = "recipes/pages/home.html"


class RecipeListViewHomeApi(RecipeListViewBase):
    template_name = "recipes/pages/home.html"

    def render_to_response(
        self,
        context: dict[str, Any],
        **response_kwargs: Any,
    ) -> HttpResponse:
        recipes = self.get_context_data()["recipes"]
        recipes_list = recipes.object_list.values()

        return JsonResponse(
            list(recipes_list),
            safe=False,
        )


class RecipeListViewCategory(RecipeListViewBase):
    template_name = "recipes/pages/category.html"

    def get_queryset(self):
        query_set = super().get_queryset()
        query_set = query_set.filter(
            category__id=self.kwargs.get("category_id"),
        )

        if not query_set:
            raise Http404()

        return query_set

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context_data = super().get_context_data(**kwargs)
        recipes = context_data.get("recipes")

        context_data.update(
            {
                "title": f"{recipes[0].category.name} - Categoria |",
            }
        )
        return context_data


class RecipeListViewSearch(RecipeListViewBase):
    template_name = "recipes/pages/search.html"

    def get_queryset(self):
        query_set = super().get_queryset()
        search_term = self.request.GET.get("search", "").strip()

        if not search_term:
            raise Http404()

        query_set = query_set.filter(
            Q(
                Q(
                    title__icontains=search_term,
                )
                | Q(
                    description__icontains=search_term,
                ),
            )
        )

        return query_set

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context_data = super().get_context_data(**kwargs)
        search_term = self.request.GET.get("search", "").strip()

        context_data.update(
            {
                "page_title": f'Search for "{search_term}" | ',
                "search_term": search_term,
                "additional_url_query": f"&search={search_term}",
            }
        )

        return context_data


class RecipeDetail(DetailView):
    model = Recipe
    context_object_name = "recipe"
    template_name = "recipes/pages/recipe-view.html"

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context_data = super().get_context_data(**kwargs)
        context_data.update(
            {
                "is_detail_page": True,
            }
        )
        return context_data

    def get_queryset(self) -> QuerySet[Any]:
        query_set = super().get_queryset()
        query_set = query_set.filter(is_published=True)
        return query_set


class RecipeDetailApi(RecipeDetail):
    def render_to_response(
        self,
        context: dict[str, Any],
        **response_kwargs: Any,
    ) -> HttpResponse:
        recipe: Recipe = self.get_context_data()["recipe"]
        recipe_dict: dict[Recipe] = model_to_dict(recipe)

        recipe_dict["created_at"] = str(recipe.created_at)
        recipe_dict["updated_at"] = str(recipe.updated_at)

        if recipe_dict.get("cover"):
            recipe_dict["cover"] = self.request.build_absolute_uri() + recipe_dict["cover"].url[1:]  # flake8: noqa
        else:
            recipe_dict["cover"] = ""

        del recipe_dict["is_published"]

        return JsonResponse(
            recipe_dict,
            safe=False,
        )


class RecipeListViewTag(RecipeListViewBase):
    template_name = "recipes/pages/search.html"

    def get_queryset(self, *args, **kwargs):
        queryset = super().get_queryset()

        queryset = queryset.filter(tags__slug=self.kwargs.get("slug", ""))

        return queryset

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        ctx = super().get_context_data(**kwargs)
        page_title = Tag.objects.filter(
            slug=self.kwargs.get("slug", ""),
        ).first()

        if not page_title:
            page_title = "No recipes found"

        page_title = f"{page_title} - Tag |"

        ctx.update({"page_title": page_title})

        return ctx
