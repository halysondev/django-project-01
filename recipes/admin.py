from django.contrib import admin

from recipes.models import Category, Recipe


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    pass


# Maneira de um relacionamento comun
# class TagInline(admin.StackedInline):
#     model = Tag

# Maneira de um relacionamento genérico
# class GenericTagInline(GenericStackedInline):
#     model = Tag

#     fields = ("name",)
#     extra = 1


@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_per_page = 10
    list_display_links = ["title"]

    list_display = [
        "id",
        "title",
        "created_at",
        "is_published",
        "author",
    ]

    search_fields = [
        "id",
        "title",
        "description",
        "slug",
        "preparation_steps",
    ]

    list_filter = [
        "category",
        "author",
        "is_published",
        "preparation_steps_is_html",
    ]

    list_editable = [
        "is_published",
    ]

    ordering = [
        "-id",
    ]

    prepopulated_fields = {
        "slug": [
            "title",
        ]
    }

    autocomplete_fields = ("tags",)
    # inlines = [
    # TagInline(), # Relacionamento comun
    # GenericTagInline, # Relacionamento genérico
    # ]
