from django.urls import resolve, reverse
from recipes import views
from .test_recipe_base import TestRecipeBase


class RecipeRecipeDetailsViewTest(TestRecipeBase):
    def test_recipe_recipe_details_views_function_is_correct(self):
        view = resolve(reverse("recipes:recipe", kwargs={"pk": 1}))
        self.assertIs(view.func.view_class, views.RecipeDetail)

    def test_recipe_recipe_details_view_returns_404_if_no_recipes_found(self):
        response = self.client.get(
            reverse(
                "recipes:recipe",
                kwargs={
                    "pk": 1000,
                },
            )
        )
        self.assertEqual(response.status_code, 404)

    def test_recipe_recipe_detail_view_returns_status_code_200_OK(self):
        self.make_recipe()

        response = self.client.get(reverse("recipes:recipe", kwargs={"pk": 1}))
        self.assertEqual(response.status_code, 200)

    def test_recipe_recipe_details_view_returns_correct_template(self):
        self.make_recipe()

        response = self.client.get(reverse("recipes:recipe", kwargs={"pk": 1}))
        self.assertTemplateUsed(response, "recipes/pages/recipe-view.html")

    def test_recipe_recipe_details_tempkates_loads_recipes(self):
        needed_title = "This is a detail page - It a load on recipe"

        self.make_recipe(title=needed_title)

        response = self.client.get(reverse("recipes:recipe", kwargs={"pk": 1}))

        content = response.content.decode("utf-8")

        self.assertIn(needed_title, content)

    def test_recipe_recipe_detail_tempkate_dont_load_recipes_not_published(
        self,
    ):  # flake8: noqa
        recipe = self.make_recipe(is_published=False)

        response = self.client.get(
            reverse(
                "recipes:recipe",
                kwargs={
                    "pk": recipe.id,
                },
            )
        )

        self.assertEqual(response.status_code, 404)
