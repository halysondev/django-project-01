from django.urls import resolve, reverse
from recipes import views
from .test_recipe_base import TestRecipeBase


class RecipeSearchViewTest(TestRecipeBase):
    def test_recipe_search_uses_correct_view_function(self):
        resolved = resolve(reverse("recipes:search"))
        self.assertIs(resolved.func.view_class, views.RecipeListViewSearch)

    def test_recipe_search_term_is_on_page_title_and_escaped(self):
        url = reverse("recipes:search") + "?search=<Teste>"
        response = self.client.get(url)
        self.assertIn(
            "Search for &quot;&lt;Teste&gt;&quot;",
            response.content.decode("utf-8"),
        )

    def test_recipe_search_raises_404_is_no_search_term(self):
        reponse = self.client.get(reverse("recipes:search"))
        self.assertEqual(reponse.status_code, 404)

    def test_recipe_search_returns_correct_template(self):
        response = self.client.get(reverse("recipes:search") + "?search=teste")

        self.assertTemplateUsed(response, "recipes/pages/search.html")

    def test_recipe_search_can_find_recipe_by_title(self):
        title1 = "This is recipe one"
        title2 = "This is recipe two"

        recipe1 = self.make_recipe(
            title=title1,
            author_data={"username": "username1"},
            slug="title-one",
        )

        recipe2 = self.make_recipe(
            title=title2,
            author_data={"username": "username2"},
            slug="title-two",
        )

        search_url = reverse("recipes:search")
        response1 = self.client.get(f"{search_url}?search={title1}")
        response2 = self.client.get(f"{search_url}?search={title2}")
        response_both = self.client.get(f"{search_url}?search=this")

        self.assertIn(recipe1, response1.context["recipes"])
        self.assertNotIn(recipe2, response1.context["recipes"])

        self.assertIn(recipe2, response2.context["recipes"])
        self.assertNotIn(recipe1, response2.context["recipes"])

        self.assertIn(recipe1, response_both.context["recipes"])
        self.assertIn(recipe2, response_both.context["recipes"])
