from unittest.mock import patch

from django.urls import resolve, reverse

from recipes import views

from .test_recipe_base import TestRecipeBase

"""Pula a classe ou o método que ele estiver decorado"""
# @skip('A mensagem do porquê eu estou pulando esse teste')


class RecipeHomeViewTest(TestRecipeBase):
    def test_recipe_home_views_function_is_correct(self):
        view = resolve(reverse("recipes:home"))
        self.assertIs(view.func.view_class, views.RecipeListViewHome)

        """Faz o teste falhar"""
        # self.fail()

    def test_recipe_home_view_returns_status_code_200_OK(self):
        response = self.client.get(reverse("recipes:home"))
        self.assertEqual(response.status_code, 200)

    def test_recipe_home_view_returns_correct_template(self):
        response = self.client.get(reverse("recipes:home"))
        self.assertTemplateUsed(response, "recipes/pages/home.html")

    def test_recipe_home_template_shows_no_recipes_if_no_recipe(self):
        response = self.client.get(reverse("recipes:home"))

        self.assertIn(
            "<h1>Não tem receitas</h1>",
            response.content.decode("utf-8"),
        )

    def test_recipe_home_templates_loads_recipes(self):
        self.make_recipe()

        response = self.client.get(reverse("recipes:home"))
        content = response.content.decode("utf-8")

        self.assertIn("title", content)
        self.assertIn("10 min", content)
        self.assertIn("5 people", content)

    def test_recipe_home_template_dont_load_recipes_not_published(self):
        self.make_recipe(is_published=False)

        response = self.client.get(reverse("recipes:home"))
        content = response.content.decode("utf-8")

        self.assertIn("<h1>Não tem receitas</h1>", content)

    @patch("recipes.views.site.PER_PAGE", new=3)
    def test_recipe_home_is_paginated(self):
        for i in range(9):
            self.make_recipe(
                title=f"titulo-{i}",
                author_data={"username": f"username{i}"},
                slug=f"title-{i}",
            )

        response = self.client.get(reverse("recipes:home"))
        recipes = response.context["recipes"]
        paginator = recipes.paginator

        self.assertEqual(paginator.num_pages, 3)
        self.assertEqual(len(paginator.get_page(1)), 3)
        self.assertEqual(len(paginator.get_page(2)), 3)
        self.assertEqual(len(paginator.get_page(3)), 3)
