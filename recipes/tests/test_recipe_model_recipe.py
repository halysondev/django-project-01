from django.core.exceptions import ValidationError
from parameterized import parameterized

from recipes.models import Recipe

from .test_recipe_base import TestRecipeBase


class RecipeModelTest(TestRecipeBase):
    def setUp(self) -> None:
        self.recipe = self.make_recipe()
        return super().setUp()

    def make_recipe_no_defaults(self):
        recipe = Recipe(
            author=self.make_author(username="new name"),
            category=self.make_category(name="Vegana"),
            title="Title for testing",
            description="description",
            slug="slug",
            preparation_time=10,
            preparation_time_unit="min",
            servings=2,
            servings_unit="peoples",
            preparation_steps="make and make and make",
        )

        recipe.full_clean()
        recipe.save()

        return recipe

    @parameterized.expand(
        [
            ("title", 65),
            ("description", 165),
            ("preparation_time_unit", 65),
            ("servings_unit", 65),
        ]
    )
    def test_recipe_field_max_lenghts(self, field, max_length):
        """Atribui um valor, mesma coisa que fazer
        self.recipe.title = 'A' * 70"""

        setattr(self.recipe, field, "A" * (max_length + 1))

        """with -> Adiciona um contexto que tem fim"""
        with self.assertRaises(ValidationError):
            self.recipe.full_clean()

    def test_recipe_preparation_steps_is_html_is_false_by_default(self):
        recipe = self.make_recipe_no_defaults()
        self.assertFalse(
            recipe.preparation_steps_is_html,
            msg="Recipe preparation_steps_is_html is not False for default",
        )

    def test_recipe_is_published_is_false_by_default(self):
        recipe = self.make_recipe_no_defaults()
        self.assertFalse(
            recipe.is_published,
            msg="Recipe is_published is not False for default",
        )

    def test_recipe_string_representation(self):
        needed = "Testing Representation"
        self.recipe.title = needed
        self.recipe.full_clean()
        self.recipe.save()
        self.assertEqual(
            str(self.recipe),
            needed,
            msg=f"Recipe string representation must be " f'"{needed}" but "{str(self.recipe)}" was received.',
        )
