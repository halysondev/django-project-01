from django.test import TestCase

from recipes import models


class TestRecipeBase(TestCase):
    """
    setUp -> executa toda vez antes de um test rodar
    tearDown -> executa toda vez depois de um test rodar
    """

    # def setUp(self) -> None:
    #     return super().setUp()

    # def tearDown(self) -> None:
    #     return super().tearDown()

    def make_category(self, name="Vegana"):
        return models.Category.objects.create(name=name)

    def make_author(
        self, first_name="first", last_name="last", username="username", email="email@email.com", password="123456"
    ):
        return models.User.objects.create_user(
            first_name=first_name, last_name=last_name, username=username, email=email, password=password
        )

    def make_recipe(
        self,
        author_data=None,
        category_data=None,
        title="title",
        description="description",
        slug="title",
        preparation_time=10,
        preparation_time_unit="min",
        servings=5,
        servings_unit="people",
        preparation_steps="make and make and make",
        preparation_steps_is_html=False,
        is_published=True,
    ):

        if author_data is None:
            author_data = {}

        if category_data is None:
            category_data = {}

        return models.Recipe.objects.create(
            author=self.make_author(**author_data),
            category=self.make_category(**category_data),
            title=title,
            description=description,
            slug=slug,
            preparation_time=preparation_time,
            preparation_time_unit=preparation_time_unit,
            servings=servings,
            servings_unit=servings_unit,
            preparation_steps=preparation_steps,
            preparation_steps_is_html=preparation_steps_is_html,
            is_published=is_published,
        )
