from django.urls import reverse
from .test_recipe_base import TestRecipeBase
from utils.pagination import make_pagination


class RecipeHomeViewTest(TestRecipeBase):
    def test_if_not_convert_str_to_int_in_pagination_url_query(self):
        url = reverse('recipes:home')

        for i in range(10):
            self.make_recipe(
                title=f'titulo-{i}',
                author_data={'username': f'username{i}'},
                slug=f'title-{i}'
            )

        response = self.client.get(url, {'page': 'abc'})

        context = response.context['pagination_range']

        self.assertEqual(context['current_page'], 1)

    def test_if_convert_str_to_int_in_pagination_url_query_return_correct_current_page(self):
        url = reverse('recipes:home')

        for i in range(10):
            self.make_recipe(
                title=f'titulo-{i}',
                author_data={'username': f'username{i}'},
                slug=f'title-{i}'
            )

        response = self.client.get(url, {'page': '2'})

        context = response.context['pagination_range']

        self.assertEqual(context['current_page'], 2)
