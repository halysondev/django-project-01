from django.urls import resolve, reverse
from recipes import views
from .test_recipe_base import TestRecipeBase


class RecipeCategoryViewTest(TestRecipeBase):
    def test_recipe_category_views_function_is_correct(self):
        view = resolve(reverse("recipes:category", kwargs={"category_id": 1}))
        self.assertIs(view.func.view_class, views.RecipeListViewCategory)

    def test_recipe_category_view_returns_404_if_no_category_found(self):
        response = self.client.get(
            reverse("recipes:category", kwargs={"category_id": 1000})
        )
        self.assertEqual(response.status_code, 404)

    def test_recipe_category_view_returns_status_code_200_OK(self):
        self.make_recipe()

        response = self.client.get(
            reverse("recipes:category", kwargs={"category_id": 1})
        )

        self.assertEqual(response.status_code, 200)

    def test_recipe_category_view_returns_correct_template(self):
        self.make_recipe()

        response = self.client.get(
            reverse("recipes:category", kwargs={"category_id": 1})
        )
        self.assertTemplateUsed(response, "recipes/pages/category.html")

    def test_recipe_category_templates_loads_recipes(self):
        needed_title = "This is a category"

        self.make_recipe(title=needed_title)

        response = self.client.get(
            reverse("recipes:category", kwargs={"category_id": 1})
        )

        content = response.content.decode("utf-8")

        self.assertIn(needed_title, content)

    def test_recipe_category_template_dont_load_recipes_not_published(self):
        recipe = self.make_recipe(is_published=False)

        response = self.client.get(
            reverse(
                "recipes:category",
                kwargs={
                    "category_id": recipe.category.id,
                },
            )
        )

        self.assertEqual(response.status_code, 404)
