import string
from collections import defaultdict
from random import SystemRandom

from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import F, Value
from django.db.models.functions import Concat
from django.urls import reverse
from django.utils.text import slugify

from tag.models import Tag


class Category(models.Model):
    name = models.CharField(max_length=65, verbose_name="Nome")

    class Meta:
        verbose_name = "Categoria"
        verbose_name_plural = "Categorias"

    def __str__(self):
        return self.name


class RecipeManager(models.Manager):
    def get_published(self):
        return (
            self.filter(is_published=True)
            .annotate(
                author__full_name=Concat(
                    F("author__first_name"),
                    Value(" "),
                    F("author__last_name"),
                    Value(" ("),
                    F("author__username"),
                    Value(")"),
                ),
            )
            .order_by("-id")
            .select_related("category", "author")
            .prefetch_related("tags")
        )


class Recipe(models.Model):
    objects = RecipeManager()

    title = models.CharField(
        max_length=50,
        verbose_name="Título",
    )
    description = models.CharField(max_length=165, verbose_name="Descrição")
    slug = models.SlugField(unique=True)
    preparation_time = models.IntegerField(verbose_name="Tempo de preparação")
    preparation_time_unit = models.CharField(
        max_length=65,
        verbose_name="Unidade do tempo de preparação",
    )
    servings = models.IntegerField(verbose_name="Servir para quantos")
    servings_unit = models.CharField(
        max_length=65,
        verbose_name="Unidade da quantidade para servir",
    )
    preparation_steps = models.TextField(
        verbose_name="Passo a passo da preparação",
    )
    preparation_steps_is_html = models.BooleanField(
        default=False, verbose_name="Passo a passo da preparação está em html?"
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_published = models.BooleanField(
        default=False,
        verbose_name="Está publicado?",
    )
    cover = models.ImageField(
        upload_to="recipes/covers/%Y/%m/%d/",
        verbose_name="Imagem da receita",
        null=True,
        blank=True,
    )
    category = models.ForeignKey(
        Category,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        default=None,
        verbose_name="Categoria",
    )
    author = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        verbose_name="Autor",
    )

    tags = models.ManyToManyField(Tag, blank=True, default="")

    class Meta:
        verbose_name = "Receita"
        verbose_name_plural = "Receitas"

    def __str__(self):
        return f"{self.title}"

    def get_absolute_url(self):
        return reverse("recipes:recipe", kwargs={"id": self.id})

    def save(self, *args, **kwargs):
        if not self.slug:
            rand_letters = "".join(
                SystemRandom().choices(
                    string.ascii_letters + string.digits,
                    k=5,
                )
            )
            self.slug = slugify(f"{self.title}-{rand_letters}")

        saved = super().save(*args, **kwargs)

        if self.cover:
            try:
                self.resize_image(self.cover, 840)
            except FileNotFoundError:
                ...

        return saved

    def clean(self, *args, **kwargs):
        error_messages = defaultdict(list)

        recipe_from_db = Recipe.objects.filter(
            title__iexact=self.title,
        ).first()

        if recipe_from_db:
            if recipe_from_db.pk != self.pk:
                error_messages["title"].append(
                    "Existe uma receita com o mesmo nome.",
                )

        if error_messages:
            raise ValidationError(error_messages)
