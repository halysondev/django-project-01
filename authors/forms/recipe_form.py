from typing import Any

from django import forms
from django.core.exceptions import ValidationError

from authors.validators import AuthorRecipeValidator
from recipes.models import Recipe
from utils.django_forms import add_attr


class AuthorRecipeForm(forms.ModelForm):
    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)

        add_attr(self.fields["preparation_steps"], "class", "span-2")

    class Meta:
        model = Recipe
        fields = [
            "cover",
            "title",
            "servings",
            "description",
            "servings_unit",
            "preparation_time",
            "preparation_steps",
            "preparation_time_unit",
        ]

        widgets = {
            "cover": forms.FileInput(attrs={"class": "span-2"}),
            "servings_unit": forms.Select(
                choices=[
                    ("Porções", "Porções"),
                    ("Pedaços", "Pedaços"),
                    ("Pessoas", "Pessoas"),
                ]
            ),
            "preparation_time_unit": forms.Select(
                choices=[
                    ("Minutos", "Minutos"),
                    ("Horas", "Horas"),
                ]
            ),
        }

    def clean(self, *args, **kwargs) -> dict[str, Any]:
        super_clean = super().clean(*args, **kwargs)

        AuthorRecipeValidator(self.cleaned_data, ErrorClass=ValidationError)

        return super_clean
