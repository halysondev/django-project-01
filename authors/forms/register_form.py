from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

from utils.django_forms import (
    add_attr,
    add_placeholder,
    strong_password,
)


class RegisterForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        add_attr(
            self.fields["username"],
            "placeholder",
            "Insert your username",
        )
        add_placeholder(self.fields["first_name"], "Ex.: John")
        add_placeholder(self.fields["last_name"], "Ex.: Doe")
        add_placeholder(self.fields["email"], "Your e-mail")
        add_placeholder(self.fields["password"], "Type your password here")
        add_placeholder(
            self.fields["confirm_password"], "Type your confirm password here"
        )

    first_name = forms.CharField(
        error_messages={
            "required": "Write your first name",
        },
        label="First name",
    )

    last_name = forms.CharField(
        error_messages={
            "required": "Write your last name",
        },
        label="Last name",
    )

    password = forms.CharField(
        widget=forms.PasswordInput(),
        validators=[strong_password],
        help_text="Password must have at least one uppercase letter, "
        "one lowercase letter and one number. The length should be "
        "at least 8 characters.",
        label="Password",
        error_messages={"required": "Password must not be empty"},
    )

    confirm_password = forms.CharField(
        widget=forms.PasswordInput(),
        label="Confirm password",
        error_messages={
            "required": "Please, repeat your password",
        },
    )

    email = forms.EmailField(
        label="Email",
        help_text="The e-mail must be valid",
    )

    username = forms.CharField(
        label="Username",
        help_text=(
            "Username must have letters, numbers or one of those @.+-_. "
            "The length should be between 4 and 150 characters."
        ),
        error_messages={
            "required": "This field must not be empty",
            "min_length": "Username must have at least 4 characters",
            "max_length": "Username must have less than 150 characters",
        },
        min_length=4,
        max_length=150,
    )

    class Meta:
        model = User
        fields = ["first_name", "last_name", "username", "email", "password"]
        """exclude -> é um campo que deixa eu excluir alguns fields do model
        na exibição do frontend """

        # exclude = ["first_name"]

        # labels = {
        #     "username": "Username",
        #     "last_name": "Last name",
        #     "email": "Email",
        # }

        # help_texts = {
        #     "email": "The e-mail must be valid.",
        # }

        # error_messages = {
        #     "username": {
        #         "required": "This field must not be empty"
        #         # "max_length": "This field must have less than
        #            3 character",
        #         # "invalid": "This field is invalid",
        #     },
        # }

        # widgets = {
        #     "password": forms.PasswordInput(
        #         attrs={
        #             "placeholder": "Type your password here",
        #             #  "class": "input text-input outra-classe",
        #         },
        #     ),
        # }

    # def clean_password(self):
    #     data = self.cleaned_data.get("password")
    #     ...
    #     """
    #         No validation error o  - code ->  é responsável por inserir
    #         no django esse code para exibir uma mensagem em relação
    #         ao code específico, já o params -> ele insere uma paramatro
    #         para a exibição no template do django
    #     """

    #     # if "atenção" in data:
    #     #   raise ValidationError(
    #     #       "Não digite %(pipoca)s no campo password",
    #     #       code="invalid",
    #     #       params={"pipoca": '"atenção"'},
    #     #   )

    #     return data

    def clean_email(self):
        email = self.cleaned_data.get("email", "")
        exist_email = User.objects.filter(email=email).exists()

        if exist_email:
            raise ValidationError(
                "User email is already in use",
                code="invalid",
            )

        # sempre lembrar de retornar o cleaned data
        return email

    def clean(self):
        cleaned_data = super().clean()

        password = cleaned_data.get("password")
        confirm_password = cleaned_data.get("confirm_password")

        if password != confirm_password:
            password_confirmation_error = ValidationError(
                "Password and Confirm password must be equal",
                code="invalid",
            )

            raise ValidationError(
                {
                    "password": password_confirmation_error,
                    "confirm_password": [
                        password_confirmation_error,
                    ],
                }
            )
