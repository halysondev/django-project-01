from unittest import TestCase
from django.test import TestCase as DjangoTestCase
from authors.forms import RegisterForm
from parameterized import parameterized
from django.urls import reverse


class AuthorRegisterFormUnitTest(TestCase):
    @parameterized.expand(
        [
            ("username", "Insert your username"),
            ("email", "Your e-mail"),
            ("first_name", "Ex.: John"),
            ("last_name", "Ex.: Doe"),
            ("password", "Type your password here"),
            ("confirm_password", "Type your confirm password here"),
        ]
    )
    def test_fields_placeholder(self, field, expected):
        form = RegisterForm()

        current_placeholder = form[field].field.widget.attrs["placeholder"]

        self.assertEqual(current_placeholder, expected)

    @parameterized.expand(
        [
            (
                "username",
                (
                    "Username must have letters, "
                    "numbers or one of those @.+-_. "
                    "The length should be between 4 and 150 characters."
                ),
            ),
            ("email", "The e-mail must be valid"),
            (
                "password",
                (
                    "Password must have at least one uppercase letter, "
                    "one lowercase letter and one number. "
                    "The length should be "
                    "at least 8 characters."
                ),
            ),
        ]
    )
    def test_fields_help_text(self, field, expected):
        form = RegisterForm()
        current = form[field].field.help_text
        self.assertEqual(current, expected)

    @parameterized.expand(
        [
            ("username", "Username"),
            ("email", "Email"),
            ("password", "Password"),
            ("confirm_password", "Confirm password"),
            ("first_name", "First name"),
            ("last_name", "Last name"),
        ]
    )
    def test_fields_label(self, field, expected):
        form = RegisterForm()
        current = form[field].field.label
        self.assertEqual(current, expected)


class AuthorRegisterFormIntegrationTest(DjangoTestCase):
    def setUp(self) -> None:
        self.formData = {
            "username": "user",
            "first_name": "first",
            "last_name": "last",
            "email": "email@email.com",
            "password": "Str0ngP@ssword1",
            "confirm_password": "Str0ngP@ssword1",
        }
        return super().setUp()

    @parameterized.expand(
        [
            ("username", "This field must not be empty"),
            ("first_name", "Write your first name"),
            ("last_name", "Write your last name"),
            ("password", "Password must not be empty"),
            ("confirm_password", "Please, repeat your password"),
        ]
    )
    def test_fields_can_not_be_empty(self, field, expected):
        self.formData[field] = ""

        url = reverse("authors:register_create")
        response = self.client.post(url, data=self.formData, follow=True)

        self.assertIn(expected, response.content.decode("utf-8"))
        self.assertIn(expected, response.context["form"].errors.get(field))

    def test_username_field_min_lenght_should_be_not_have_less_4(self):
        self.formData["username"] = "abc"

        url = reverse("authors:register_create")

        response = self.client.post(url, data=self.formData, follow=True)

        self.assertIn(
            "Username must have at least 4 characters",
            response.content.decode("utf-8"),
        )

        self.assertIn(
            "Username must have at least 4 characters",
            response.context["form"].errors.get("username"),
        )

    def test_username_field_max_lenght_should_be_not_have_more_150(self):
        self.formData["username"] = "a" * 151

        url = reverse("authors:register_create")

        response = self.client.post(url, data=self.formData, follow=True)

        self.assertIn(
            "Username must have less than 150 characters",
            response.content.decode("utf-8"),
        )

        self.assertIn(
            "Username must have less than 150 characters",
            response.context["form"].errors.get("username"),
        )

    def test_password_field_have_lower_upper_case_letters_and_numbers(self):
        self.formData["password"] = "Abc123"

        url = reverse("authors:register_create")

        response = self.client.post(url, data=self.formData, follow=True)

        self.assertIn(
            "Password must have at least one uppercase letter, "
            "one lowercase letter and one number. The length should be "
            "at least 8 characters.",
            response.content.decode("utf-8"),
        )

        self.assertIn(
            "Password must have at least one uppercase letter, "
            "one lowercase letter and one number. The length should be "
            "at least 8 characters.",
            response.context["form"].errors.get("password"),
        )

        self.formData["password"] = "ABCabc123@"
        response = self.client.post(url, data=self.formData, follow=True)

        self.assertNotIn(
            "Password must have at least one uppercase letter, "
            "one lowercase letter and one number. The length should be "
            "at least 8 characters.",
            response.context["form"].errors.get("password"),
        )

    def test_password_and_confirm_password_are_equal(self):
        self.formData["password"] = "ABCabc123@"
        self.formData["confirm_password"] = "ABCabc123@5"

        url = reverse("authors:register_create")

        response = self.client.post(url, data=self.formData, follow=True)

        msg_expected = "Password and Confirm password must be equal"

        self.assertIn(
            msg_expected,
            response.content.decode("utf-8"),
        )

        self.assertIn(
            msg_expected,
            response.context["form"].errors.get("password"),
        )

        self.assertIn(
            msg_expected,
            response.context["form"].errors.get("confirm_password"),
        )

        self.formData["confirm_password"] = "ABCabc123@"

        response = self.client.post(url, data=self.formData, follow=True)

        self.assertNotIn(
            msg_expected,
            response.content.decode("utf-8"),
        )

    def test_send_get_to_registration_create_view_should_be_returns_404(self):
        url = reverse("authors:register_create")

        response = self.client.get(url)

        self.assertEqual(response.status_code, 404)

    def test_email_field_must_be_unique(self):
        url = reverse("authors:register_create")
        self.client.post(url, data=self.formData, follow=True)

        response = self.client.post(url, data=self.formData, follow=True)

        msg = "User email is already in use"

        self.assertIn(
            msg,
            response.context["form"].errors.get("email"),
        )

        self.assertIn(
            msg,
            response.content.decode("utf-8"),
        )

    def test_author_created_can_login(self):
        url = reverse("authors:register_create")

        self.formData.update(
            {
                "username": "testuser",
                "password": "@Bc123456",
                "confirm_password": "@Bc123456",
            }
        )

        self.client.post(url, data=self.formData, follow=True)

        is_authenticated = self.client.login(
            username="testuser",
            password="@Bc123456",
        )

        self.assertTrue(is_authenticated)
