from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.http import Http404
from django.shortcuts import redirect, render
from django.urls import reverse

from authors.forms import LoginForm, RegisterForm
from recipes.models import Recipe


def register_view(request):
    register_form_data = request.session.get("register_form_data", None)
    form = RegisterForm(register_form_data)
    return render(
        request,
        "authors/pages/register_view.html",
        {
            "form": form,
            "form_action": reverse("authors:register_create"),
        },
    )


def register_create(request):
    if not request.POST:
        raise Http404()

    POST = request.POST
    request.session["register_form_data"] = POST
    form = RegisterForm(POST)

    if form.is_valid():
        """Pegar os dados para alterar depois"""
        # data = form.save(commit=False)
        # data.outro_campo = "new_value"
        # data.save()
        form_unsaved = form.save(commit=False)
        form_unsaved.set_password(form_unsaved.password)
        form_unsaved.save()

        messages.success(request, "Your user is created, please log in")

        del request.session["register_form_data"]
        return redirect(reverse("authors:login"))

    return redirect("authors:register")


def login_view(request):
    form = LoginForm()

    return render(
        request,
        "authors/pages/login_view.html",
        context={
            "form": form,
            "form_action": reverse("authors:login_create"),
        },
    )


def login_create(request):
    if not request.POST:
        raise Http404()

    form = LoginForm(request.POST)

    if form.is_valid():
        authenticated_user = authenticate(
            username=form.cleaned_data.get("username", ""),
            password=form.cleaned_data.get("password", ""),
        )

        if authenticated_user is not None:
            messages.success(request, "You are logged in")
            login(request, authenticated_user)
        else:
            messages.error(request, "Invalid credential")
    else:
        messages.error(request, "Invalid username or password")

    return redirect(reverse("authors:dashboard"))


@login_required(login_url="authors:login")
def logout_view(request):
    login_url = reverse("authors:login")

    if not request.POST:
        messages.error(request, "Invalid logout user")
        return redirect(login_url)

    if request.POST.get("username") != request.user.username:
        messages.error(request, "Invalid logout user")
        return redirect(login_url)

    messages.success(request, "Logged out successfully")
    logout(request)
    return redirect(login_url)


@login_required(login_url="authors:login")
def dashboard_view(request):
    recipes = Recipe.objects.filter(is_published=False, author=request.user)

    return render(
        request,
        "authors/pages/dashboard_view.html",
        context={
            "recipes": recipes,
        },
    )
